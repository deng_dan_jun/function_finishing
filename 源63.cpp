#include<string>
#include<iostream>
#include<algorithm>
#include<numeric>
#include<iterator>
#include<functional>
#include<chrono>

using std::chrono::duration_cast;
using std::chrono::high_resolution_clock;
using std::chrono::milliseconds;
using std::find_if_not;
using std::logical_and;
using std::bind;
using std::greater_equal;
using std::less_equal;
using std::placeholders::_1;
using std::make_reverse_iterator;
using std::find_if;
using std::accumulate;
using std::cout;
using std::string;
using std::stoi;


void test1(const char *ch,const int len,int &number)
{
	if (!ch || len <= 0)
		return;
	int index{ -1 }, num{ 1 };
	const char *begin{ ch }, *end{ ch + len }, *numberEnd;
	do
	{
		if (*begin >= '0' && *begin <= '9')
			break;
	} while (++begin != end);
	if (begin == end)
		return;
	numberEnd = begin;
	do
	{
		if (*numberEnd < '0' || *numberEnd > '9')
			break;
	} while (++numberEnd != end);
	for (--numberEnd; numberEnd >= begin; --numberEnd)
	{
		if (++index)num *= 10;
		number += (*numberEnd - '0')*num;
	}
}

void test2(const string &source, int &number)
{
	if (source.empty())
		return;
	int index{ -1 }, num{ 1 };
	string::const_iterator iterBegin{ source.cbegin() }, iterEnd{ source.cend() }, numberEnd;
	if ((iterBegin = find_if(iterBegin, iterEnd, bind(logical_and<>(), bind(greater_equal<>(), _1, '0'), bind(less_equal<>(), _1, '9')))) == iterEnd)
		return;
	numberEnd = find_if_not(iterBegin + 1, iterEnd, bind(logical_and<>(), bind(greater_equal<>(), _1, '0'), bind(less_equal<>(), _1, '9')));
	number = stoi(string(iterBegin, numberEnd));
}


void test3(const char *ch, const int len, int &number)
{
	if (!ch || len <= 0)
		return;
	sscanf(ch, "%*[a-z]%d%*[a-z]",  &number);
}


void test4(const char *ch, const int len, int &number)
{
	if (!ch || len <= 0)
		return;
	int index{ -1 }, num{ 1 };
	const char *begin{ ch }, *end{ ch + len }, *numberEnd;
	do
	{
		if (*begin >= '0' && *begin <= '9')
			break;
	} while (++begin != end);
	if (begin == end)
		return;
	numberEnd = begin;
	do
	{
		if (*numberEnd < '0' || *numberEnd > '9')
			break;
	} while (++numberEnd != end);
	char *temp = (char*)malloc(numberEnd - begin + 1);
	memcpy(temp, begin, numberEnd - begin);
	temp[numberEnd - begin] = 0;
	number=atoi(temp);
	free(temp);
}


void test5(const string &source, int &number)
{
	if (source.empty())
		return;
	int index{ -1 }, num{ 1 };
	string::const_iterator iterBegin{ source.cbegin() }, iterEnd{ source.cend() }, numberEnd;
	if ((iterBegin = find_if(iterBegin, iterEnd, bind(logical_and<>(), bind(greater_equal<>(), _1, '0'), bind(less_equal<>(), _1, '9')))) == iterEnd)
		return;
	numberEnd = find_if_not(iterBegin + 1, iterEnd, bind(logical_and<>(), bind(greater_equal<>(), _1, '0'), bind(less_equal<>(), _1, '9')));
	accumulate(make_reverse_iterator(numberEnd), make_reverse_iterator(iterBegin), &number, [&index, &num](auto *sum, auto const ch)
	{
		if (++index)num *= 10;
		*sum += (ch - '0')*num;
		return sum;
	});
}

int main()
{
	int number{};
	const char *ch{ "dwdwefwe9568fewfew" };
	size_t len{ strlen(ch) };
	string str{ "dwdwefwe9568fewfew" };

	{
		test1(ch, len, number);
		cout << number << '\n';
	}

	{
		number = 0;
		test2(str, number);
		cout<< number << '\n';
	}

	{
		number = 0;
		test3(ch, len, number);
		cout << number << '\n';
	}

	{
		number = 0;
		test4(ch, len, number);
		cout << number << '\n';
	}

	{
		number = 0;
		test5(str, number);
		cout << number << '\n';
	}

	auto t1{ high_resolution_clock::now() }, t2{ high_resolution_clock::now() };
	int i{};

	t1 = high_resolution_clock::now();
	while (++i != 100000)
	{
		number = 0;
		test1(ch, len, number);
	}
	t2 = high_resolution_clock::now();
	cout << "test1:higher C use " << duration_cast<milliseconds>(t2 - t1).count() << " ms \n";

	i = 0;
	t1 = high_resolution_clock::now();
	while (++i != 100000)
	{
		number = 0;
		test2(str, number);
	}
	t2 = high_resolution_clock::now();
	cout << "test2:stoi  use " << duration_cast<milliseconds>(t2 - t1).count() << " ms \n";

	i = 0;
	t1 = high_resolution_clock::now();
	while (++i != 100000)
	{
		number = 0;
		test3(ch, len, number);
	}
	t2 = high_resolution_clock::now();
	cout << "test3:sscanf use " << duration_cast<milliseconds>(t2 - t1).count() << " ms \n";

	i = 0;
	t1 = high_resolution_clock::now();
	while (++i != 100000)
	{
		number = 0;
		test4(ch, len, number);
	}
	t2 = high_resolution_clock::now();
	cout << "test4:atoi  use " << duration_cast<milliseconds>(t2 - t1).count() << " ms \n";

	i = 0;
	t1 = high_resolution_clock::now();
	while (++i != 100000)
	{
		number = 0;
		test5(str, number);
	}
	t2 = high_resolution_clock::now();
	cout << "test5:accumulate  use " << duration_cast<milliseconds>(t2 - t1).count() << " ms \n";

	return 0;
}


/*

9568
9568
9568
9568
9568
test1:higher C use 1 ms
test2:stoi  use 7 ms
test3:sscanf use 52 ms
test4:atoi  use 11 ms
test5:accumulate  use 1 ms

*/