#include<string>
#include<iostream>
#include<algorithm>
#include<numeric>
#include<iterator>
#include<functional>
#include<chrono>
#include<vector>
#include<map>
#include<cstring>
#include<map>

using std::pair;
using std::distance;
using std::find;
using std::pair;
using std::vector;
using std::chrono::duration_cast;
using std::chrono::high_resolution_clock;
using std::chrono::milliseconds;
using std::find_if_not;
using std::logical_and;
using std::bind;
using std::greater_equal;
using std::less_equal;
using std::placeholders::_1;
using std::make_reverse_iterator;
using std::find_if;
using std::accumulate;
using std::cout;
using std::string;
using std::stoi;
using std::not_equal_to;
using std::equal_to;
using std::greater;
using std::all_of;
using std::stof;
using std::logical_or;
using std::less;
using std::mismatch;


//  为了方便大家理解，最下方有一个翻译成原生指针的版本 findMaxFastC，功能上和findMaxFast一样，性能上findMaxFast更佳

//  两个数字，先比较符号，然后比较大小，比较大小方法为先比较整数区域的不为0的字段长度，如果相等则进行比对小数部分
//  众所周知，C语言最强的算法是纯指针间操作实现的，竭尽全力避免中间变量产生以提升性能（这才是C真正让其他高级语言无法追赶性能的关键所在）
//  这里用STL模仿比较在字符串中存在的最大数字的功能，使用迭代器在位置间灵活比较大小
//  支持正负数，正负浮点数比对 0   目前不支持对于0的负号判断



void findMaxatof(const char* source, const int len, float &result, bool &success)   //  两个数字，先比较符号，然后比较大小，比较大小方法为先比较整数区域的不为0的字段长度，如果相等则进行比对小数部分
{
	result = 0;
	success = false;
	if (!source || len <= 0)
		return;
	const char *iterBegin{ source }, *iterEnd{ source + len }, *numberLeftBegin, *numberLeftEnd, *numberRightBegin, *numberRightEnd;

	float otherNumber{};
	char *newplace{};
	int newSize{};

	do
	{
		if ((*iterBegin >= '0' && *iterBegin <= '9'))
			break;
	} while (++iterBegin != iterEnd);

	if (iterBegin == iterEnd)
		return;

	numberLeftBegin = iterBegin;

	success = true;

	while (++iterBegin != iterEnd)
	{
		if ((*iterBegin < '0' || *iterBegin > '9'))
			break;
	}

	numberLeftEnd = iterBegin;

	if (*numberLeftEnd == '.' && numberLeftEnd + 1 != iterEnd && (*(numberLeftEnd + 1) >= '0' && *(numberLeftEnd + 1) <= '9'))
	{
		iterBegin = numberLeftEnd + 1;
		do
		{
			if ((*iterBegin < '0' || *iterBegin > '9'))
				break;
		} while (++iterBegin != iterEnd);

		numberLeftEnd = iterBegin;
	}

	newSize = numberLeftEnd - numberLeftBegin;
	if (!(newplace = (char*)malloc(newSize + 1)))
	{
		success = false;
		return;
	}
	memcpy(newplace, numberLeftBegin, newSize);
	newplace[newSize] = 0;
	result = atof(newplace);
	if (numberLeftBegin != source && *(numberLeftBegin - 1) == '-')
		result *= -1.0f;

	if (numberLeftEnd == iterEnd)
	{
		free(newplace);
		return;
	}

	iterBegin = numberLeftEnd + 1;

	while (iterBegin != iterEnd)
	{
		do
		{
			if ((*iterBegin >= '0' && *iterBegin <= '9'))
				break;
		} while (++iterBegin != iterEnd);

		if (iterBegin == iterEnd)
			break;

		numberRightBegin = iterBegin;

		while (++iterBegin != iterEnd)
		{
			if ((*iterBegin < '0' || *iterBegin > '9'))
				break;
		}

		numberRightEnd = iterBegin;

		if (numberRightEnd != iterEnd && *numberRightEnd == '.' && numberRightEnd + 1 != iterEnd && (*(numberRightEnd + 1) >= '0' && *(numberRightEnd + 1) <= '9'))
		{
			while (++iterBegin != iterEnd)
			{
				if ((*iterBegin < '0' || *iterBegin > '9'))
					break;
			}

			numberRightEnd = iterBegin;
		}

		if (!(newSize > numberRightEnd - numberRightBegin))
		{
			free(newplace);
			newSize = numberRightEnd - numberRightBegin+1;
			if (!(newplace = (char*)malloc(newSize)))
			{
				success = false;
				return;
			}
			memcpy(newplace, numberRightBegin, newSize-1);
			newplace[newSize-1] = 0;
		}
		else
		{
			memcpy(newplace, numberRightBegin, numberRightEnd - numberRightBegin);
			newplace[numberRightEnd - numberRightBegin] = 0;
		}

		otherNumber = atof(newplace);
		if (*(numberRightBegin - 1) == '-')
			otherNumber *= -1.0f;

		if (result < otherNumber)
			result = otherNumber;

		if (iterBegin != iterEnd)
			++iterBegin;
	}

	free(newplace);
	return;
}






void findMaxstof(const char* source, const int len, float &result, bool &success)   //  两个数字，先比较符号，然后比较大小，比较大小方法为先比较整数区域的不为0的字段长度，如果相等则进行比对小数部分
{
	result = 0;
	success = false;
	if (!source || len <= 0)
		return;
	const char *iterBegin{ source }, *iterEnd{ source + len }, *numberLeftBegin, *numberLeftEnd, *numberRightBegin, *numberRightEnd;

	float otherNumber{};
	string temp;

	do
	{
		if ((*iterBegin >= '0' && *iterBegin <= '9'))
			break;
	} while (++iterBegin != iterEnd);

	if (iterBegin == iterEnd)
		return;

	numberLeftBegin = iterBegin;

	success = true;

	while (++iterBegin != iterEnd)
	{
		if ((*iterBegin < '0' || *iterBegin > '9'))
			break;
	}

	numberLeftEnd = iterBegin;

	if (*numberLeftEnd == '.' && numberLeftEnd + 1 != iterEnd && (*(numberLeftEnd + 1) >= '0' && *(numberLeftEnd + 1) <= '9'))
	{
		iterBegin = numberLeftEnd + 1;
		do
		{
			if ((*iterBegin < '0' || *iterBegin > '9'))
				break;
		} while (++iterBegin != iterEnd);

		numberLeftEnd = iterBegin;
	}

	temp.assign(numberLeftBegin, numberLeftEnd);
	result = std::stof(temp);
	if (numberLeftBegin != source && *(numberLeftBegin - 1) == '-')
		result *= -1.0f;

	if (numberLeftEnd == iterEnd)
	{
		return;
	}

	iterBegin = numberLeftEnd + 1;

	while (iterBegin != iterEnd)
	{
		do
		{
			if ((*iterBegin >= '0' && *iterBegin <= '9'))
				break;
		} while (++iterBegin != iterEnd);

		if (iterBegin == iterEnd)
			break;

		numberRightBegin = iterBegin;

		while (++iterBegin != iterEnd)
		{
			if ((*iterBegin < '0' || *iterBegin > '9'))
				break;
		}

		numberRightEnd = iterBegin;

		if (numberRightEnd != iterEnd && *numberRightEnd == '.' && numberRightEnd + 1 != iterEnd && (*(numberRightEnd + 1) >= '0' && *(numberRightEnd + 1) <= '9'))
		{
			while (++iterBegin != iterEnd)
			{
				if ((*iterBegin < '0' || *iterBegin > '9'))
					break;
			}

			numberRightEnd = iterBegin;
		}

		temp.assign(numberRightBegin, numberRightEnd);

		otherNumber = stof(temp);
		if (*(numberRightBegin - 1) == '-')
			otherNumber *= -1.0f;

		if (result < otherNumber)
			result = otherNumber;

		if (iterBegin != iterEnd)
			++iterBegin;
	}

	return;
}



void findMaxtoFloat(const char* source, const int len, float &result, bool &success)   //  两个数字，先比较符号，然后比较大小，比较大小方法为先比较整数区域的不为0的字段长度，如果相等则进行比对小数部分
{
	result = 0;
	success = false;
	if (!source || len <= 0)
		return;
	const char *iterBegin{ source }, *iterEnd{ source + len }, *numberLeftBegin, *numberLeftEnd, *numberRightBegin, *numberRightEnd, *temp;

	float otherNumber{};
	float num{ 1.0f };
	int index{ -1 };

	do
	{
		if ((*iterBegin >= '0' && *iterBegin <= '9'))
			break;
	} while (++iterBegin != iterEnd);

	if (iterBegin == iterEnd)
		return;

	numberLeftBegin = iterBegin;

	success = true;

	while (++iterBegin != iterEnd)
	{
		if ((*iterBegin < '0' || *iterBegin > '9'))
			break;
	}

	numberLeftEnd = iterBegin;

	temp = numberLeftEnd;
	while (--temp >= numberLeftBegin)
	{
		if (++index)num *= 10.0f;
		result += (*temp - '0')*num;
	}

	if (*numberLeftEnd == '.' && numberLeftEnd + 1 != iterEnd && (*(numberLeftEnd + 1) >= '0' && *(numberLeftEnd + 1) <= '9'))
	{
		index = -1;
		num = 0.1f;
		temp = numberLeftEnd;

		iterBegin = numberLeftEnd + 1;
		do
		{
			if ((*iterBegin < '0' || *iterBegin > '9'))
				break;
		} while (++iterBegin != iterEnd);

		numberLeftEnd = iterBegin;

		while (++temp != numberLeftEnd)
		{
			if (++index)num *= 0.1f;
			result += (*temp - '0')*num;
		}
	}

	if (numberLeftBegin != source && *(numberLeftBegin - 1) == '-')
		result *= -1.0f;

	if (numberLeftEnd == iterEnd)
	{
		return;
	}

	iterBegin = numberLeftEnd + 1;

	while (iterBegin != iterEnd)
	{
		do
		{
			if ((*iterBegin >= '0' && *iterBegin <= '9'))
				break;
		} while (++iterBegin != iterEnd);

		if (iterBegin == iterEnd)
			break;

		numberRightBegin = iterBegin;

		while (++iterBegin != iterEnd)
		{
			if ((*iterBegin < '0' || *iterBegin > '9'))
				break;
		}

		numberRightEnd = iterBegin;

		index = -1;
		num = 1.0f;
		temp = numberRightEnd;
		otherNumber = 0.0f;
		while (--temp >= numberRightBegin)
		{
			if (++index)num *= 10.0f;
			otherNumber += (*temp - '0')*num;
		}


		if (numberRightEnd != iterEnd && *numberRightEnd == '.' && numberRightEnd + 1 != iterEnd && (*(numberRightEnd + 1) >= '0' && *(numberRightEnd + 1) <= '9'))
		{
			index = -1;
			num = 0.1f;
			temp = numberRightEnd;

			while (++iterBegin != iterEnd)
			{
				if ((*iterBegin < '0' || *iterBegin > '9'))
					break;
			}

			numberRightEnd = iterBegin;

			while (++temp != numberRightEnd)
			{
				if (++index)num *= 0.1f;
				otherNumber += (*temp - '0')*num;
			}

		}

		if (*(numberRightBegin - 1) == '-')
			otherNumber *= -1.0f;

		if (result < otherNumber)
			result = otherNumber;

		if (iterBegin != iterEnd)
			++iterBegin;
	}

	return;
}








//首先判断正负符号，
//然后判断整数长度，
//整数长度一样的，比对整数大小

//整数大小一致的，判断有没有小数
//然后判断小数起始点位置长度，
//位置长度一样的，查找小数位最后一位不为0的地方，保存长度
//比对长度，然后根据长度确定比较范围，最后确定大小
//默认左边为最大值，如果右边大，则保存

void findMaxFast(const string &source, float &result, bool &success)
{
	result = 0;
	success = false;
	if (source.empty())
		return;
	string::const_iterator iterBegin{ source.cbegin() }, iterEnd{ source.cend() }, 
		numberLeftBegin, numberLeftEnd, numberLeftDot,     // 左边数字开始位置  结束位置  小数点位置
		numberRightBegin, numberRightEnd, numberRightDot,  // 右边数字开始位置  结束位置  小数点位置
		PositiveBeginLeft, PositiveBeginRight,             // 整数部分不为0的位置
		DecimalBeginLeft, DecimalBeginRight,               // 小数部分从小数点开始数第一个不为0的位置 
		DecimalEndLeft, DecimalEndRight;                   // 小数部分从末尾开始向前数第一个不为0的位置
	bool hasDotLeft{ false }, hasDotRight{ false },      //标记是否有小数点
		hasPositiveLeft{ false }, hasPositiveRight{ false },           //标记是否解析了整数部分首个不为0位置
		hasDecimalBeginLeft{ false }, hasDecimalBeginRight{ false },   //是否已经解析小数点后第一个不为0位置
		hasDecimalEndLeft{ false }, hasDecimalEndRight{ false },       //是否已经解析末尾往前第一个不为0位置
		isPositiveLeft{false}, isPositiveRight{false};                 //是否为正数
	pair<string::const_iterator, string::const_iterator>matchPair;
	int PositiveLenLeft{}, PositiveLenRight{},                      //有效整数位置
		DecimalBeginLenLeft{}, DecimalBeginLenRight{},              //小数点到小数点后第一个不为0的位置
		DecimalEndLenLeft{}, DecimalEndLenRight{};                  //小数点第一个不为0位置到最后一个不为0位置后一位的距离

	int index{ -1 };
	float num{ 1.0f };
	//首先获取Begin end ，判断有没有点
	if ((numberLeftBegin = find_if(iterBegin, iterEnd, bind(logical_and<>(), bind(greater_equal<>(), _1, '0'), bind(less_equal<>(), _1, '9')))) == iterEnd)
		return;

	if (distance(source.cbegin(), numberLeftBegin) && *(numberLeftBegin - 1) == '-')
		isPositiveLeft = false;
	else
		isPositiveLeft = true;

	success = true;

	numberLeftEnd = find_if_not(numberLeftBegin + 1, iterEnd, bind(logical_and<>(), bind(greater_equal<>(), _1, '0'), bind(less_equal<>(), _1, '9')));

	if (distance(numberLeftEnd, iterEnd) < 3)
	{
		accumulate(make_reverse_iterator(numberLeftEnd), make_reverse_iterator(numberLeftBegin), &result, [&index, &num](auto *sum, auto const ch)
		{
			if (++index)num *= 10.0f;
			*sum += (ch - '0')*num;
			return sum;
		});
		if (!isPositiveLeft)
			result *= -1.0f;
		return;
	}
	if (*numberLeftEnd == '.' && (*(numberLeftEnd + 1) >= '0' && *(numberLeftEnd + 1) <= '9'))
	{
		hasDotLeft = true;
		numberLeftDot = numberLeftEnd;
		numberLeftEnd = find_if_not(numberLeftDot + 1, iterEnd, bind(logical_and<>(), bind(greater_equal<>(), _1, '0'), bind(less_equal<>(), _1, '9')));
		if (numberLeftEnd == iterEnd)
		{
			accumulate(make_reverse_iterator(numberLeftDot), make_reverse_iterator(numberLeftBegin), &result, [&index, &num](auto *sum, auto const ch)
			{
				if (++index)num *= 10.0f;
				*sum += (ch - '0')*num;
				return sum;
			});
			num = 0.1f;
			index = -1;
			accumulate(numberLeftDot + 1, numberLeftEnd, &result, [&index, &num](auto *sum, auto const ch)
			{
				if (++index)num *= 0.1f;
				*sum += (ch - '0')*num;
				return sum;
			});
			if (!isPositiveLeft)
				result *= -1.0f;
			return;
		}
	}

	iterBegin = numberLeftEnd + 1;


	//首先判断正负符号，                                             isPositiveLeft          isPositiveRight
	//然后判断整数长度，                                             PositiveBeginLeft       PositiveBeginRight       PositiveLenLeft     PositiveLenRight     hasPositiveLeft      hasPositive2  
	//整数长度一样的，比对整数大小
	//整数大小一致的，判断有没有小数                     hasDecimalBeginLeft    hasDecimalBeginRight    DecimalBeginLeft   DecimalBeginRight   DecimalBeginLenLeft   DecimalBeginLenRight              
	//然后判断小数起始点位置长度，
	//位置长度一样的，查找小数位最后一位不为0的地方，保存长度   hasDecimalEndLeft    hasDecimalEndRight    DecimalEndLeft   DecimalEndRight   DecimalEndLenLeft   DecimalEndLenRight  
	//比对长度，然后根据长度确定比较范围，最后确定大小
	while (iterBegin != iterEnd)
	{
		hasDotRight = hasPositiveRight = hasDecimalBeginRight = hasDecimalEndRight = false;

		if ((numberRightBegin = find_if(iterBegin, iterEnd, bind(logical_and<>(), bind(greater_equal<>(), _1, '0'), bind(less_equal<>(), _1, '9')))) == iterEnd)
			break;

		if (*(numberRightBegin - 1) == '-')
			isPositiveRight = false;
		else
			isPositiveRight = true;

		numberRightEnd = find_if_not(numberRightBegin + 1, iterEnd, bind(logical_and<>(), bind(greater_equal<>(), _1, '0'), bind(less_equal<>(), _1, '9')));

		if (distance(numberRightEnd, iterEnd) > 2 && *numberRightEnd == '.' && (*(numberRightEnd + 1) >= '0' && *(numberRightEnd + 1) <= '9'))
		{
			hasDotRight = true;
			numberRightDot = numberRightEnd;
			numberRightEnd = find_if_not(numberRightDot + 1, iterEnd, bind(logical_and<>(), bind(greater_equal<>(), _1, '0'), bind(less_equal<>(), _1, '9')));
		}



		//  首先比较符号  +  -
		if (isPositiveLeft != isPositiveRight)
		{
			if (isPositiveLeft && !isPositiveRight)
			{
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
			if (!isPositiveLeft && isPositiveRight)
			{
				hasDotLeft = hasDotRight;
				hasPositiveLeft = hasPositiveRight;
				isPositiveLeft = isPositiveRight;
				hasDecimalBeginLeft = hasDecimalBeginRight;
				hasDecimalEndLeft = hasDecimalEndRight;
				numberLeftBegin = numberRightBegin;
				if (hasDotLeft)
					numberLeftDot = numberRightDot;
				numberLeftEnd = numberRightEnd;
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
		}

		//判断整数长度
		if (!hasPositiveLeft)
		{
			PositiveBeginLeft = find_if(numberLeftBegin, hasDotLeft ? numberLeftDot : numberLeftEnd, bind(not_equal_to<>(), _1, '0'));
			PositiveLenLeft = distance(PositiveBeginLeft, hasDotLeft ? numberLeftDot : numberLeftEnd);
			hasPositiveLeft = true;
		}


		PositiveBeginRight = find_if(numberRightBegin, hasDotRight ? numberRightDot : numberRightEnd, bind(not_equal_to<>(), _1, '0'));
		PositiveLenRight = distance(PositiveBeginRight, hasDotRight ? numberRightDot : numberRightEnd);
		hasPositiveRight = true;


		//比对整数部分长度
		if (PositiveLenLeft != PositiveLenRight)
		{
			if (PositiveLenLeft < PositiveLenRight)   //  <
			{
				if (isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//if (hasPositiveLeft)
					//{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
			else          //  PositiveLenLeft > PositiveLenRight    >
			{
				if (!isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
		}

		//长度一致比对大小,有整数，无整数
		if (PositiveLenLeft == PositiveLenRight)
		{
			if (PositiveLenLeft)        // 有整数情况  无整数情况意味大家一样大
			{
				matchPair = mismatch(PositiveBeginLeft, hasDotLeft ? numberLeftDot : numberLeftEnd, PositiveBeginRight, hasDotRight ? numberRightDot : numberRightEnd);
				if (matchPair.first != (hasDotLeft ? numberLeftDot : numberLeftEnd))        // 相等时整数大小一致，只考虑不想等情况
				{
					if (*matchPair.first > *matchPair.second)
					{
						if (!isPositiveRight)
						{
							hasDotLeft = hasDotRight;
							hasPositiveLeft = hasPositiveRight;
							isPositiveLeft = isPositiveRight;
							hasDecimalBeginLeft = hasDecimalBeginRight;
							hasDecimalEndLeft = hasDecimalEndRight;
							numberLeftBegin = numberRightBegin;
							if (hasDotLeft)
								numberLeftDot = numberRightDot;
							//			if (hasPositiveLeft)
							//			{
							PositiveBeginLeft = PositiveBeginRight;
							PositiveLenLeft = PositiveLenRight;
							//			}
							numberLeftEnd = numberRightEnd;
						}
						iterBegin = numberRightEnd;
						if (iterBegin != iterEnd)
							++iterBegin;
						continue;
					}
					else                      //        *matchPair.first < *matchPair.second
					{
						if (isPositiveRight)
						{
							hasDotLeft = hasDotRight;
							hasPositiveLeft = hasPositiveRight;
							isPositiveLeft = isPositiveRight;
							hasDecimalBeginLeft = hasDecimalBeginRight;
							hasDecimalEndLeft = hasDecimalEndRight;
							numberLeftBegin = numberRightBegin;
							if (hasDotLeft)
								numberLeftDot = numberRightDot;
							//			if (hasPositiveLeft)
							//			{
							PositiveBeginLeft = PositiveBeginRight;
							PositiveLenLeft = PositiveLenRight;
							//			}
							numberLeftEnd = numberRightEnd;
						}
						iterBegin = numberRightEnd;
						if (iterBegin != iterEnd)
							++iterBegin;
						continue;
					}
				}
			}
		}




		//   比对小数情况
		//   首先比对首个不为0的位置长度情况   
		//   首先考虑有没有小数
		if (!hasDotLeft && !hasDotRight)  //都没有小数
		{
			iterBegin = numberRightEnd;
			if (iterBegin != iterEnd)
				++iterBegin;
			continue;
		}


		// 假设有小数，那么需要考虑若干种情况：
		// 1 一个有小数，另一个无  有小数者 可能是全0  不全为0

		if (!hasDecimalBeginLeft)
		{
			if (hasDotLeft)
			{
				DecimalBeginLeft = find_if(numberLeftDot + 1, numberLeftEnd, bind(not_equal_to<>(), _1, '0'));
			}
			DecimalBeginLenLeft = hasDotLeft ? (DecimalBeginLeft != numberLeftEnd ? distance(numberLeftDot, DecimalBeginLeft) : 0) : 0;
			hasDecimalBeginLeft = true;
		}

		if (hasDotRight)
		{
			DecimalBeginRight = find_if(numberRightDot + 1, numberRightEnd, bind(not_equal_to<>(), _1, '0'));
		}
		DecimalBeginLenRight = hasDotRight ? (DecimalBeginRight != numberRightEnd ? distance(numberRightDot, DecimalBeginRight) : 0) : 0;
		hasDecimalBeginRight = true;


		//如果都为0，也就是说无小数部分
		if (!DecimalBeginLenLeft && !hasDecimalBeginRight)
		{
			iterBegin = numberRightEnd;
			if (iterBegin != iterEnd)
				++iterBegin;
			continue;
		}




		//   首先考虑长度不一致的情况，距离越小者越大
		if (DecimalBeginLenLeft != DecimalBeginLenRight)
		{
			if (!DecimalBeginLenLeft && hasDecimalBeginRight)   //  <
			{
				if (isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
			if (DecimalBeginLenLeft && !hasDecimalBeginRight)   //   >
			{
				if (!isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
			if (DecimalBeginLenLeft < DecimalBeginLenRight)
			{
				if (isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
			else     //   DecimalBeginLenLeft > DecimalBeginLenRight
			{
				if (!isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
		}


		//   DecimalEndLeft   DecimalEndRight   DecimalEndLenLeft   DecimalEndLenRight 
		//  小数区间比较，经过上面的比对筛选，剩下都是有小数的
		//  只考虑三种情况  大  小  等于      
		if (!DecimalEndLenLeft)
		{
			DecimalEndLeft = find_if(make_reverse_iterator(numberLeftEnd), make_reverse_iterator(DecimalBeginLeft), bind(not_equal_to<>(), _1, '0')).base();
			DecimalEndLenLeft = distance(DecimalBeginLeft, DecimalEndLeft);
			DecimalEndLenLeft = true;
		}


		DecimalEndRight = find_if(make_reverse_iterator(numberRightEnd), make_reverse_iterator(DecimalBeginRight), bind(not_equal_to<>(), _1, '0')).base();
		DecimalEndLenRight = distance(DecimalBeginRight, DecimalEndRight);
		DecimalEndLenRight = true;


		if (DecimalEndLenLeft == DecimalEndLenRight)
		{
			matchPair = mismatch(DecimalBeginLeft, DecimalEndLeft, DecimalBeginRight, DecimalEndRight);
			if (matchPair.first == DecimalEndLeft)
			{
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
			if (*matchPair.first > *matchPair.second)     //    
			{
				if (!isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					//		if (hasDecimalEndLeft)
					//		{
					DecimalEndLeft = DecimalEndRight;
					DecimalEndLenLeft = DecimalEndLenRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
			else    //   *matchPair.first < *matchPair.second     //
			{
				if (!isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					//		if (hasDecimalEndLeft)
					//		{
					DecimalEndLeft = DecimalEndRight;
					DecimalEndLenLeft = DecimalEndLenRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
		}
		if (DecimalEndLenLeft < DecimalEndLenRight)
		{
			matchPair = mismatch(DecimalBeginLeft, DecimalEndLeft, DecimalBeginRight, DecimalBeginRight + DecimalEndLenLeft);
			if (matchPair.first == DecimalEndLeft)   //   <
			{
				if (!isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					//		if (hasDecimalEndLeft)
					//		{
					DecimalEndLeft = DecimalEndRight;
					DecimalEndLenLeft = DecimalEndLenRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
			if (*matchPair.first > *matchPair.second)     //    
			{
				if (!isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					//		if (hasDecimalEndLeft)
					//		{
					DecimalEndLeft = DecimalEndRight;
					DecimalEndLenLeft = DecimalEndLenRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
			else    //   *matchPair.first < *matchPair.second     //
			{
				if (!isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					//		if (hasDecimalEndLeft)
					//		{
					DecimalEndLeft = DecimalEndRight;
					DecimalEndLenLeft = DecimalEndLenRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
		}
		if (DecimalEndLenLeft > DecimalEndLenRight)
			//else    //   DecimalEndLenLeft > DecimalEndLenRight
		{
			matchPair = mismatch(DecimalBeginLeft, DecimalBeginLeft + DecimalEndLenRight, DecimalBeginRight, DecimalEndRight);
			if (matchPair.first == DecimalEndLeft)   //   >
			{
				if (!isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					//		if (hasDecimalEndLeft)
					//		{
					DecimalEndLeft = DecimalEndRight;
					DecimalEndLenLeft = DecimalEndLenRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
			if (*matchPair.first > *matchPair.second)     //    
			{
				if (!isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					//		if (hasDecimalEndLeft)
					//		{
					DecimalEndLeft = DecimalEndRight;
					DecimalEndLenLeft = DecimalEndLenRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
			else    //   *matchPair.first < *matchPair.second     //
			{
				if (!isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					//		if (hasDecimalEndLeft)
					//		{
					DecimalEndLeft = DecimalEndRight;
					DecimalEndLenLeft = DecimalEndLenRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
		}
	}


	if (hasDotLeft)
	{
		accumulate(make_reverse_iterator(numberLeftDot), make_reverse_iterator(numberLeftBegin), &result, [&index, &num](auto *sum, auto const ch)
		{
			if (++index)num *= 10.0f;
			*sum += (ch - '0')*num;
			return sum;
		});
		num = 0.1f;
		index = -1;
		accumulate(numberLeftDot + 1, numberLeftEnd, &result, [&index, &num](auto *sum, auto const ch)
		{
			if (++index)num *= 0.1f;
			*sum += (ch - '0')*num;
			return sum;
		});
		if (distance(source.cbegin(), numberLeftBegin) && *(numberLeftBegin - 1) == '-')
			result *= -1.0f;
		return;
	}
	accumulate(make_reverse_iterator(numberLeftEnd), make_reverse_iterator(numberLeftBegin), &result, [&index, &num](auto *sum, auto const ch)
	{
		if (++index)num *= 10.0f;
		*sum += (ch - '0')*num;
		return sum;
	});
	if (distance(source.cbegin(), numberLeftBegin) && *(numberLeftBegin - 1) == '-')
		result *= -1.0f;
	return;
}




void findMaxFastC(const char* source, const unsigned int len, float &result, bool &success);




int main()
{
	float number{};
	bool success;
	int index{ -1 };

	const char *ch1{ "dwdwefwe9568fewfew95631fwefwef36999.25636grg" }, *ch2{ "dwdwefwe-9568fewfew+95631fwefwef000" }, *ch3{ "dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462" },
		*ch4{ "dwdwefwe-9568.5151551ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefes" }, *ch5{ "dwdwefwe-9568.5151551ewfew-9568wefwef" }, *ch6{ "dwdwefwe+9568.5151551ewfew9569wefwef" },
		*ch7{ "dwdwefwe09568fewfew0085631fwefwef" }, *ch8{ "dwdwefwe-9568.5151551ewfew-9568.515155wefwef" }, *ch9{ "ergegerge-98563.1gwsreger9568" },
		*ch10{ "dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
		dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
		dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
		dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
		dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
		dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
		dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
		dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
		dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
		dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
		dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
		dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568\
dwdwefwe9568fewfew95631fwefwef36999.25636grgdwdwefwe-9568fewfew+95631fwefwef000dwdwefwe-9568.5623ewfew956.8569fwefwef0085456.85462dwdwefwe-9568.5151551\
		ewfew-9568.25365wefwef000ewfewfe00.5626fefew-5.3262sefesdwdwefwe-9568.5151551ewfew-9568wefwefdwdwefwe+9568.5151551ewfew9569wefwefdwdwefwe09568fewfew0085631fwef\
        wefdwdwefwe-9568.5151551ewfew-9568.515155wefwefergegerge-98563.1gwsreger9568" };

	const size_t len1{ strlen(ch1) }, len2{ strlen(ch2) }, len3{ strlen(ch3) }, len4{ strlen(ch4) }, len5{ strlen(ch5) }, len6{ strlen(ch6) }, len7{ strlen(ch7) },
		len8{ strlen(ch8) }, len9{ strlen(ch9) }, len10{ strlen(ch10) };


	vector<string>vec{ ch1,ch2,ch3,ch4,ch5,ch6,ch7,ch8,ch9,ch10 };


	cout << "findMaxatof:\n";
	findMaxatof(ch1, len1, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}

	findMaxatof(ch2, len2, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}

	findMaxatof(ch3, len3, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}

	findMaxatof(ch4, len4, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}

	findMaxatof(ch5, len5, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}

	findMaxatof(ch6, len6, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}

	findMaxatof(ch7, len7, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}

	findMaxatof(ch8, len8, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}

	findMaxatof(ch9, len9, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}

	findMaxatof(ch10, len10, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}


	cout << "findMaxstof:\n";
	index = -1;
	findMaxstof(ch1, len1, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}

	findMaxstof(ch2, len2, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}

	findMaxstof(ch3, len3, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}

	findMaxstof(ch4, len4, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}

	findMaxstof(ch5, len5, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}

	findMaxstof(ch6, len6, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}

	findMaxstof(ch7, len7, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}

	findMaxstof(ch8, len8, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}

	findMaxstof(ch9, len9, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}

	findMaxstof(ch10, len10, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}


	cout << "findMaxtoFloat:\n";
	index = -1;
	findMaxtoFloat(ch1, len1, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}

	findMaxtoFloat(ch2, len2, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}

	findMaxtoFloat(ch3, len3, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}

	findMaxtoFloat(ch4, len4, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}

	findMaxtoFloat(ch5, len5, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}

	findMaxtoFloat(ch6, len6, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}

	findMaxtoFloat(ch7, len7, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}

	findMaxtoFloat(ch8, len8, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}

	findMaxtoFloat(ch9, len9, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}

	findMaxtoFloat(ch10, len10, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}



	cout << "findMaxFast:\n";
	index = -1;
	for (auto const &str : vec)
	{
		++index;
		findMaxFast(str, number, success);
		if (success)
		{
			cout << index << "   " << number << '\n';
		}
	}


	cout << "findMaxFastC:\n";
	index = -1;

	findMaxFastC(ch1, len1, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}

	findMaxFastC(ch2, len2, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}


	findMaxFastC(ch3, len3, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}

	findMaxFastC(ch4, len4, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}


	findMaxFastC(ch5, len5, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}

	findMaxFastC(ch6, len6, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}

	findMaxFastC(ch7, len7, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}

	findMaxFastC(ch8, len8, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}

	findMaxFastC(ch9, len9, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}

	findMaxFastC(ch10, len10, number, success);
	++index;
	if (success)
	{
		cout << index << "   " << number << '\n';
	}






	auto t1{ high_resolution_clock::now() }, t2{ high_resolution_clock::now() };


	
	index = -1;
	t1 = high_resolution_clock::now();
	while (++index != 100000)
	{
		findMaxatof(ch1, len1, number, success);
		findMaxatof(ch2, len2, number, success);
		findMaxatof(ch3, len3, number, success);
		findMaxatof(ch4, len4, number, success);
		findMaxatof(ch5, len5, number, success);
		findMaxatof(ch6, len6, number, success);
		findMaxatof(ch7, len7, number, success);
		findMaxatof(ch8, len8, number, success);
		findMaxatof(ch9, len9, number, success);
		findMaxatof(ch10, len10, number, success);
	}
	t2 = high_resolution_clock::now();
	cout << "atof use " << duration_cast<milliseconds>(t2 - t1).count() << " ms \n";


	index = -1;
	t1 = high_resolution_clock::now();
	while (++index != 100000)
	{
		findMaxstof(ch1, len1, number, success);
		findMaxstof(ch2, len2, number, success);
		findMaxstof(ch3, len3, number, success);
		findMaxstof(ch4, len4, number, success);
		findMaxstof(ch5, len5, number, success);
		findMaxstof(ch6, len6, number, success);
		findMaxstof(ch7, len7, number, success);
		findMaxstof(ch8, len8, number, success);
		findMaxstof(ch9, len9, number, success);
		findMaxstof(ch10, len10, number, success);
	}
	t2 = high_resolution_clock::now();
	cout << "stof use " << duration_cast<milliseconds>(t2 - t1).count() << " ms \n";
	


	index = -1;
	t1 = high_resolution_clock::now();
	while (++index != 100000)
	{
		findMaxFast(vec[0], number, success);
		findMaxFast(vec[1], number, success);
		findMaxFast(vec[2], number, success);
		findMaxFast(vec[3], number, success);
		findMaxFast(vec[4], number, success);
		findMaxFast(vec[5], number, success);
		findMaxFast(vec[6], number, success);
		findMaxFast(vec[7], number, success);
		findMaxFast(vec[8], number, success);
		findMaxFast(vec[9], number, success);
	}
	t2 = high_resolution_clock::now();
	cout << "STL use " << duration_cast<milliseconds>(t2 - t1).count() << " ms \n";


	index = -1;
	t1 = high_resolution_clock::now();
	while (++index != 100000)
	{
		findMaxtoFloat(ch1, len1, number, success);
		findMaxtoFloat(ch2, len2, number, success);
		findMaxtoFloat(ch3, len3, number, success);
		findMaxtoFloat(ch4, len4, number, success);
		findMaxtoFloat(ch5, len5, number, success);
		findMaxtoFloat(ch6, len6, number, success);
		findMaxtoFloat(ch7, len7, number, success);
		findMaxtoFloat(ch8, len8, number, success);
		findMaxtoFloat(ch9, len9, number, success);
		findMaxtoFloat(ch10, len10, number, success);
	}
	t2 = high_resolution_clock::now();
	cout << "toFloat use " << duration_cast<milliseconds>(t2 - t1).count() << " ms \n";


	index = -1;
	t1 = high_resolution_clock::now();
	while (++index != 100000)
	{
		findMaxFastC(ch1, len1, number, success);
		findMaxFastC(ch2, len2, number, success);
		findMaxFastC(ch3, len3, number, success);
		findMaxFastC(ch4, len4, number, success);
		findMaxFastC(ch5, len5, number, success);
		findMaxFastC(ch6, len6, number, success);
		findMaxFastC(ch7, len7, number, success);
		findMaxFastC(ch8, len8, number, success);
		findMaxFastC(ch9, len9, number, success);
		findMaxFastC(ch10, len10, number, success);
	}
	t2 = high_resolution_clock::now();
	cout << "FastC use " << duration_cast<milliseconds>(t2 - t1).count() << " ms \n";


	return 0;
}










//  findMaxFast  STL版本函数翻译成C指针版本，看不懂STL版本的可以对照着看这个，功能是一样的，但性能慢于STL版本

void findMaxFastC(const char* source, const unsigned int len, float &result, bool &success)
{
	result = 0;
	success = false;
	if (!source || !len)
		return;
	const char *iterBegin{ source }, *iterEnd{ source + len }, *numberLeftBegin{}, *numberLeftEnd{}, *numberLeftDot{}, *numberRightBegin{}, *numberRightEnd{}, *numberRightDot{},
		*PositiveBeginLeft{}, *PositiveBeginRight{}, *PositiveEndLeft{}, *PositiveEndRight{}, *compareLeft{}, *compareRight{}, *DecimalBeginLeft{}, *DecimalBeginRight{}, *DecimalEndLeft{},
		*DecimalEndRight{};
	bool hasDotLeft{ false }, hasDotRight{ false }, hasPositiveLeft{ false }, hasPositiveRight{ false }, hasDecimalBeginLeft{ false }, hasDecimalBeginRight{ false },
		hasDecimalEndLeft{ false }, hasDecimalEndRight{ false }, isPositiveLeft{}, isPositiveRight{};
	int PositiveLenLeft{}, PositiveLenRight{}, DecimalBeginLenLeft{}, DecimalBeginLenRight{}, DecimalEndLenLeft{}, DecimalEndLenRight{};

	int index{ -1 };
	float num{ 1.0f };
	//首先获取Begin end ，判断有没有点

	while (iterBegin != iterEnd)
	{
		if (*iterBegin >= '0' && *iterBegin <= '9')
			break;
		++iterBegin;
	}

	if (iterBegin == iterEnd)
		return;

	numberLeftBegin = iterBegin;


	if (numberLeftBegin - source && *(numberLeftBegin - 1) == '-')
		isPositiveLeft = false;
	else
		isPositiveLeft = true;

	success = true;

	while (++iterBegin != iterEnd)
	{
		if (*iterBegin<'0' || *iterBegin>'9')
			break;
	}

	numberLeftEnd = iterBegin;

	if (distance(numberLeftEnd, iterEnd) < 3)
	{
		while (--numberLeftEnd >= numberLeftBegin)
		{
			if (++index)num *= 10.0f;
			result += (*numberLeftEnd - '0')*num;
		}
		if (!isPositiveLeft)
			result *= -1.0f;
		return;

	}
	if (*numberLeftEnd == '.' && (*(numberLeftEnd + 1) >= '0' && *(numberLeftEnd + 1) <= '9'))
	{
		hasDotLeft = true;
		numberLeftDot = numberLeftEnd;

		while (++numberLeftEnd != iterEnd)
		{
			if (*numberLeftEnd<'0' || *numberLeftEnd>'9')
				break;
		}

		if (numberLeftEnd == iterEnd)
		{
			iterBegin = numberLeftDot;
			while (--iterBegin >= numberLeftBegin)
			{
				if (++index)num *= 10.0f;
				result += (*iterBegin - '0')*num;
			}

			num = 0.1f;
			index = -1;

			while (++numberLeftDot != numberLeftEnd)
			{
				if (++index)num *= 0.1f;
				result += (*numberLeftDot - '0')*num;
			}

			if (!isPositiveLeft)
				result *= -1.0f;
			return;
		}
	}

	iterBegin = numberLeftEnd + 1;




	//首先判断正负符号，                                             isPositiveLeft          isPositiveRight
	//然后判断整数长度，                                             PositiveBeginLeft       PositiveBeginRight       PositiveLenLeft     PositiveLenRight     hasPositiveLeft      hasPositive2  
	//整数长度一样的，比对整数大小
	//整数大小一致的，判断有没有小数                     hasDecimalBeginLeft    hasDecimalBeginRight    DecimalBeginLeft   DecimalBeginRight   DecimalBeginLenLeft   DecimalBeginLenRight              
	//然后判断小数起始点位置长度，
	//位置长度一样的，查找小数位最后一位不为0的地方，保存长度   hasDecimalEndLeft    hasDecimalEndRight    DecimalEndLeft   DecimalEndRight   DecimalEndLenLeft   DecimalEndLenRight  
	//比对长度，然后根据长度确定比较范围，最后确定大小

	while (iterBegin != iterEnd)
	{
		hasDotRight = hasPositiveRight = hasDecimalBeginRight = hasDecimalEndRight = false;

		while (iterBegin != iterEnd)
		{
			if (*iterBegin >= '0' && *iterBegin <= '9')
				break;
			++iterBegin;
		}

		if (iterBegin == iterEnd)
			break;

		numberRightBegin = iterBegin;

		if (*(numberRightBegin - 1) == '-')
			isPositiveRight = false;
		else
			isPositiveRight = true;

		while (++iterBegin != iterEnd)
		{
			if (*iterBegin<'0' || *iterBegin>'9')
				break;
		}

		numberRightEnd = iterBegin;

		if (iterEnd - numberRightEnd > 2 && *numberRightEnd == '.' && (*(numberRightEnd + 1) >= '0' && *(numberRightEnd + 1) <= '9'))
		{
			hasDotRight = true;
			numberRightDot = numberRightEnd;

			while (++iterBegin != iterEnd)
			{
				if (*iterBegin<'0' || *iterBegin>'9')
					break;
			}
			numberRightEnd = iterBegin;
		}


		//  首先比较符号
		if (isPositiveLeft != isPositiveRight)
		{
			if (isPositiveLeft && !isPositiveRight)
			{
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
			if (!isPositiveLeft && isPositiveRight)
			{
				hasDotLeft = hasDotRight;
				hasPositiveLeft = hasPositiveRight;
				isPositiveLeft = isPositiveRight;
				hasDecimalBeginLeft = hasDecimalBeginRight;
				hasDecimalEndLeft = hasDecimalEndRight;
				numberLeftBegin = numberRightBegin;
				if (hasDotLeft)
					numberLeftDot = numberRightDot;
				numberLeftEnd = numberRightEnd;
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
		}

		//判断整数长度
		if (!hasPositiveLeft)
		{
			PositiveBeginLeft = numberLeftBegin;
			if (hasDotLeft)
			{
				while (PositiveBeginLeft != numberLeftDot)
				{
					if (*PositiveBeginLeft != '0')
						break;
					++PositiveBeginLeft;
				}
				PositiveLenLeft = numberLeftDot - PositiveBeginLeft;
			}
			else
			{
				while (PositiveBeginLeft != numberLeftEnd)
				{
					if (*PositiveBeginLeft != '0')
						break;
					++PositiveBeginLeft;
				}
				PositiveLenLeft = numberLeftEnd - PositiveBeginLeft;
			}
			hasPositiveLeft = true;
		}



		PositiveBeginRight = numberRightBegin;
		if (hasDotRight)
		{
			while (PositiveBeginRight != numberRightDot)
			{
				if (*PositiveBeginRight != '0')
					break;
				++PositiveBeginRight;
			}
			PositiveLenRight = numberRightDot - PositiveBeginRight;
		}
		else
		{
			while (PositiveBeginRight != numberRightEnd)
			{
				if (*PositiveBeginRight != '0')
					break;
				++PositiveBeginRight;
			}
			PositiveLenRight = numberRightEnd - PositiveBeginRight;
		}
		hasPositiveRight = true;


		//比对长度
		if (PositiveLenLeft != PositiveLenRight)
		{
			if (PositiveLenLeft < PositiveLenRight)   //  <
			{
				if (isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//if (hasPositiveLeft)
					//{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
			else          //  PositiveLenLeft > PositiveLenRight    >
			{
				if (!isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
		}

		//长度一致比对大小,有整数，无整数
		if (PositiveLenLeft == PositiveLenRight)
		{
			if (PositiveLenLeft)        // 有整数情况  无整数情况意味大家一样大
			{
				if (hasDotLeft)
					PositiveEndLeft = numberLeftDot;
				else
					PositiveEndLeft = numberLeftEnd;

				if (hasDotRight)
					PositiveEndRight = numberRightDot;
				else
					PositiveEndRight = numberRightEnd;

				compareLeft = PositiveBeginLeft, compareRight = PositiveBeginRight;


				while (compareLeft != PositiveEndLeft)
				{
					if (*compareLeft != *compareRight)
						break;
					++compareLeft;
					++compareRight;
				}


				if (compareLeft != PositiveEndLeft)        // 相等时整数大小一致，只考虑不想等情况
				{
					if (*compareLeft > *compareRight)
					{
						if (!isPositiveRight)
						{
							hasDotLeft = hasDotRight;
							hasPositiveLeft = hasPositiveRight;
							isPositiveLeft = isPositiveRight;
							hasDecimalBeginLeft = hasDecimalBeginRight;
							hasDecimalEndLeft = hasDecimalEndRight;
							numberLeftBegin = numberRightBegin;
							if (hasDotLeft)
								numberLeftDot = numberRightDot;
							//			if (hasPositiveLeft)
							//			{
							PositiveBeginLeft = PositiveBeginRight;
							PositiveLenLeft = PositiveLenRight;
							//			}
							numberLeftEnd = numberRightEnd;
						}
						iterBegin = numberRightEnd;
						if (iterBegin != iterEnd)
							++iterBegin;
						continue;
					}
					else                      //        *matchPair.first < *matchPair.second
					{
						if (isPositiveRight)
						{
							hasDotLeft = hasDotRight;
							hasPositiveLeft = hasPositiveRight;
							isPositiveLeft = isPositiveRight;
							hasDecimalBeginLeft = hasDecimalBeginRight;
							hasDecimalEndLeft = hasDecimalEndRight;
							numberLeftBegin = numberRightBegin;
							if (hasDotLeft)
								numberLeftDot = numberRightDot;
							//			if (hasPositiveLeft)
							//			{
							PositiveBeginLeft = PositiveBeginRight;
							PositiveLenLeft = PositiveLenRight;
							//			}
							numberLeftEnd = numberRightEnd;
						}
						iterBegin = numberRightEnd;
						if (iterBegin != iterEnd)
							++iterBegin;
						continue;
					}
				}
			}
		}




		//   比对小数情况
		//   首先比对首个不为0的位置长度情况   
		//   首先考虑有没有小数
		if (!hasDotLeft && !hasDotRight)  //都没有小数
		{
			iterBegin = numberRightEnd;
			if (iterBegin != iterEnd)
				++iterBegin;
			continue;
		}


		// 假设有小数，那么需要考虑若干种情况：
		// 1 一个有小数，另一个无  有小数者 可能是全0  不全为0

		if (!hasDecimalBeginLeft)
		{
			if (hasDotLeft)
			{
				DecimalBeginLeft = numberLeftDot + 1;
				while (DecimalBeginLeft != numberLeftEnd)
				{
					if (*DecimalBeginLeft != '0')
						break;
					++DecimalBeginLeft;
				}
				if (DecimalBeginLeft != numberLeftEnd)
				{
					DecimalBeginLenLeft = DecimalBeginLeft - numberLeftDot;
				}
				else
				{
					DecimalBeginLenLeft = 0;
				}
			}
			else
			{
				DecimalBeginLenLeft = 0;
			}

			hasDecimalBeginLeft = true;
		}



		if (hasDotRight)
		{
			DecimalBeginRight = numberRightDot + 1;
			while (DecimalBeginRight != numberRightEnd)
			{
				if (*DecimalBeginRight != '0')
					break;
			}
			if (DecimalBeginRight != numberRightEnd)
			{
				DecimalBeginLenRight = DecimalBeginRight - numberRightDot;
			}
			else
			{
				DecimalBeginLenRight = 0;
			}
		}
		else
		{
			DecimalBeginLenRight = 0;
		}
		hasDecimalBeginRight = true;


		//如果都为0，也就是说无小数部分
		if (!DecimalBeginLenLeft && !hasDecimalBeginRight)
		{
			iterBegin = numberRightEnd;
			if (iterBegin != iterEnd)
				++iterBegin;
			continue;
		}




		//   首先考虑长度不一致的情况，距离越小者越大
		if (DecimalBeginLenLeft != DecimalBeginLenRight)
		{
			if (!DecimalBeginLenLeft && hasDecimalBeginRight)   //  <
			{
				if (isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
			if (DecimalBeginLenLeft && !hasDecimalBeginRight)   //   >
			{
				if (!isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
			if (DecimalBeginLenLeft < DecimalBeginLenRight)
			{
				if (isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
			else     //   DecimalBeginLenLeft > DecimalBeginLenRight
			{
				if (!isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
		}




		//   DecimalEndLeft   DecimalEndRight   DecimalEndLenLeft   DecimalEndLenRight 
		//  小数区间比较，经过上面的比对筛选，剩下都是有小数的
		//  只考虑三种情况  大  小  等于      
		if (!DecimalEndLenLeft)
		{
			DecimalEndLeft = numberLeftEnd - 1;
			while (DecimalEndLeft != DecimalBeginLeft)
			{
				if (*DecimalEndLeft != '0')
					break;
			}
			++DecimalEndLeft;

			DecimalEndLenLeft = DecimalEndLeft - DecimalBeginLeft;
			DecimalEndLenLeft = true;
		}


		DecimalEndRight = numberRightEnd - 1;
		while (DecimalEndRight != DecimalBeginRight)
		{
			if (*DecimalEndRight != '0')
				break;
		}
		++DecimalEndRight;

		DecimalEndLenRight = DecimalEndRight - DecimalBeginRight;
		DecimalEndLenRight = true;





		if (DecimalEndLenLeft == DecimalEndLenRight)
		{
			compareLeft = DecimalBeginLeft, compareRight = DecimalBeginRight;

			while (compareLeft != DecimalEndLeft)
			{
				if (*compareLeft != *compareRight)
					break;
				++compareLeft;
				++compareRight;
			}

			if (compareLeft == DecimalEndLeft)
			{
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
			if (*compareLeft > *compareRight)     //    
			{
				if (!isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					//		if (hasDecimalEndLeft)
					//		{
					DecimalEndLeft = DecimalEndRight;
					DecimalEndLenLeft = DecimalEndLenRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
			else    //   *matchPair.first < *matchPair.second     //
			{
				if (!isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					//		if (hasDecimalEndLeft)
					//		{
					DecimalEndLeft = DecimalEndRight;
					DecimalEndLenLeft = DecimalEndLenRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
		}
		if (DecimalEndLenLeft < DecimalEndLenRight)
		{
			compareLeft = DecimalBeginLeft, compareRight = DecimalBeginRight;

			while (compareLeft != DecimalEndLeft)
			{
				if (*compareLeft != *compareRight)
					break;
				++compareLeft;
				++compareRight;
			}


			if (compareLeft == DecimalEndLeft)   //   <
			{
				if (!isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					//		if (hasDecimalEndLeft)
					//		{
					DecimalEndLeft = DecimalEndRight;
					DecimalEndLenLeft = DecimalEndLenRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
			if (*compareLeft > *compareRight)     //    
			{
				if (!isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					//		if (hasDecimalEndLeft)
					//		{
					DecimalEndLeft = DecimalEndRight;
					DecimalEndLenLeft = DecimalEndLenRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
			else    //   *matchPair.first < *matchPair.second     //
			{
				if (!isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					//		if (hasDecimalEndLeft)
					//		{
					DecimalEndLeft = DecimalEndRight;
					DecimalEndLenLeft = DecimalEndLenRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
		}
		if (DecimalEndLenLeft > DecimalEndLenRight)
			//else    //   DecimalEndLenLeft > DecimalEndLenRight
		{
			compareLeft = DecimalBeginLeft, compareRight = DecimalBeginRight;

			while (compareRight != DecimalEndRight)
			{
				if (*compareLeft != *compareRight)
					break;
				++compareLeft;
				++compareRight;
			}


			if (compareRight == DecimalEndRight)   //   >
			{
				if (!isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					//		if (hasDecimalEndLeft)
					//		{
					DecimalEndLeft = DecimalEndRight;
					DecimalEndLenLeft = DecimalEndLenRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
			if (*compareLeft > *compareRight)     //    
			{
				if (!isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					//		if (hasDecimalEndLeft)
					//		{
					DecimalEndLeft = DecimalEndRight;
					DecimalEndLenLeft = DecimalEndLenRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
			else    //   *matchPair.first < *matchPair.second     //
			{
				if (!isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					//		if (hasDecimalEndLeft)
					//		{
					DecimalEndLeft = DecimalEndRight;
					DecimalEndLenLeft = DecimalEndLenRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
		}
	}


	if (hasDotLeft)
	{
		iterBegin = numberLeftDot;
		while (--iterBegin >= numberLeftBegin)
		{
			if (++index)num *= 10.0f;
			result += (*iterBegin - '0')*num;
		}


		num = 0.1f;
		index = -1;

		while (++numberLeftDot != numberLeftEnd)
		{
			if (++index)num *= 0.1f;
			result += (*numberLeftDot - '0')*num;
		}

		if (numberLeftBegin - source && *(numberLeftBegin - 1) == '-')
			result *= -1.0f;
		return;
	}

	while (--numberLeftEnd >= numberLeftBegin)
	{
		if (++index)num *= 10.0f;
		result += (*numberLeftEnd - '0')*num;
	}

	if (numberLeftBegin - source && *(numberLeftBegin - 1) == '-')
		result *= -1.0f;
	return;
}




/*

msvc 2017最大优化与gcc9.1 O3 下测试结果均为atof性能的10倍（字符串越长差距将越大，常规的atof使用过程需要频繁进行new）
经过优化，目前在gcc 9.1 O3下比转数字略微快一点，

测试cpu为3代i5，在6代i7上测试了结果为慢于转float比较，但在linux虚拟机上仍然快于，具体速度大家测试，这个比对方法用在大数比较就好
centos 6.8  gcc 9.1  O3下结果为：

10万次
msvc2017

atof    30029ms
stof    26963ms
STL     1912ms
tofloat 2274ms


gcc 9.1  centos 6.8  O3    -Wl,--strip-all

"g++" -W"switch" -W"no-deprecated-declarations" -W"empty-body" -W"conversion" -W"return-type" -W"parentheses" -W"no-pointer-sign" -W"no-format" -W"uninitialized" -W"unreachable-code" -W"unused-function" -W"unused-value" -W"unused-variable" -std=c++17 -w -fno-strict-aliasing -g0 "g++" -O3 "3600000" -fthreadsafe-statics -D "NDEBUG" -W"switch" -W"no-deprecated-declarations" -W"empty-body" -W"conversion" -W"return-type" -W"parentheses" -W"no-format" -W"uninitialized" -W"unreachable-code" -W"unused-function" -W"unused-value" -W"unused-variable" -frtti -fomit-frame-pointer -std=c11 -fexceptions "1" -o "C:\Users\predator\source\repos\linux1\linux1\obj\x64\Release\%(filename).o"
-o"C:\Users\predator\source\repos\linux1\linux1\bin\x64\Release\linux1.out" "3600000" -Wl,-z,relro -Wl,--strip-all -Wl,-z,noexecstack -Wl,--no-undefined "g++" -Wl,-z,now


atof    11633ms
stof    8909ms
STL     1184ms
tofloat 1601ms
FastC   1291ms





*/



//位运算版本

void findMaxFast2(const string &source, float &result, bool &success)
{
	result = 0;
	success = false;
	if (source.empty())
		return;
	string::const_iterator iterBegin{ source.cbegin() }, iterEnd{ source.cend() },
		numberLeftBegin, numberLeftEnd, numberLeftDot,     // 左边数字开始位置  结束位置  小数点位置
		numberRightBegin, numberRightEnd, numberRightDot,  // 右边数字开始位置  结束位置  小数点位置
		PositiveBeginLeft, PositiveBeginRight,             // 整数部分不为0的位置
		DecimalBeginLeft, DecimalBeginRight,               // 小数部分从小数点开始数第一个不为0的位置 
		DecimalEndLeft, DecimalEndRight;                   // 小数部分从末尾开始向前数第一个不为0的位置
	bool hasDotLeft{ false }, hasDotRight{ false },      //标记是否有小数点
		hasPositiveLeft{ false }, hasPositiveRight{ false },           //标记是否解析了整数部分首个不为0位置
		hasDecimalBeginLeft{ false }, hasDecimalBeginRight{ false },   //是否已经解析小数点后第一个不为0位置
		hasDecimalEndLeft{ false }, hasDecimalEndRight{ false },       //是否已经解析末尾往前第一个不为0位置
		isPositiveLeft{ false }, isPositiveRight{ false };                 //是否为正数
	pair<string::const_iterator, string::const_iterator>matchPair;
	int PositiveLenLeft{}, PositiveLenRight{},                      //有效整数位置
		DecimalBeginLenLeft{}, DecimalBeginLenRight{},              //小数点到小数点后第一个不为0的位置
		DecimalEndLenLeft{}, DecimalEndLenRight{};                  //小数点第一个不为0位置到最后一个不为0位置后一位的距离

	int index{ -1 };
	float num{ 1.0f };
	//首先获取Begin end ，判断有没有点
	if ((numberLeftBegin = find_if(iterBegin, iterEnd, bind(logical_and<>(), bind(greater<>(), _1, '/'), bind(less<>(), _1, ':')))) == iterEnd)
		return;

	//!(x ^ y)
	//if (distance(source.cbegin(), numberLeftBegin) && *(numberLeftBegin - 1) == '-')
	if (distance(source.cbegin(), numberLeftBegin) && !(*(numberLeftBegin - 1) ^ '-'))
		isPositiveLeft = false;
	else
		isPositiveLeft = true;

	success = true;

	numberLeftEnd = find_if_not(numberLeftBegin + 1, iterEnd, bind(logical_and<>(), bind(greater<>(), _1, '/'), bind(less<>(), _1, ':')));

	if (((distance(numberLeftEnd, iterEnd) - 3) >> 31))
		//if (distance(numberLeftEnd, iterEnd) < 3)
	{
		accumulate(make_reverse_iterator(numberLeftEnd), make_reverse_iterator(numberLeftBegin), &result, [&index, &num](auto *sum, auto const ch)
		{
			if (++index)num *= 10.0f;
			*sum += (ch - '0')*num;
			return sum;
		});
		if (!isPositiveLeft)
			result *= -1.0f;
		return;
	}
	if (!(*numberLeftEnd ^ '.') && (!((*(numberLeftEnd + 1) - '/') >> 31) && ((*(numberLeftEnd + 1) - ':') >> 31)))
		//if (*numberLeftEnd == '.' && (*(numberLeftEnd + 1) >= '0' && *(numberLeftEnd + 1) <= '9'))
	{
		hasDotLeft = true;
		numberLeftDot = numberLeftEnd;
		numberLeftEnd = find_if_not(numberLeftDot + 1, iterEnd, bind(logical_and<>(), bind(greater<>(), _1, '/'), bind(less<>(), _1, ':')));
		if (numberLeftEnd == iterEnd)
		{
			accumulate(make_reverse_iterator(numberLeftDot), make_reverse_iterator(numberLeftBegin), &result, [&index, &num](auto *sum, auto const ch)
			{
				if (++index)num *= 10.0f;
				*sum += (ch - '0')*num;
				return sum;
			});
			num = 0.1f;
			index = -1;
			accumulate(numberLeftDot + 1, numberLeftEnd, &result, [&index, &num](auto *sum, auto const ch)
			{
				if (++index)num *= 0.1f;
				*sum += (ch - '0')*num;
				return sum;
			});
			if (!isPositiveLeft)
				result *= -1.0f;
			return;
		}
	}

	iterBegin = numberLeftEnd + 1;


	//首先判断正负符号，                                             isPositiveLeft          isPositiveRight
	//然后判断整数长度，                                             PositiveBeginLeft       PositiveBeginRight       PositiveLenLeft     PositiveLenRight     hasPositiveLeft      hasPositive2  
	//整数长度一样的，比对整数大小
	//整数大小一致的，判断有没有小数                     hasDecimalBeginLeft    hasDecimalBeginRight    DecimalBeginLeft   DecimalBeginRight   DecimalBeginLenLeft   DecimalBeginLenRight              
	//然后判断小数起始点位置长度，
	//位置长度一样的，查找小数位最后一位不为0的地方，保存长度   hasDecimalEndLeft    hasDecimalEndRight    DecimalEndLeft   DecimalEndRight   DecimalEndLenLeft   DecimalEndLenRight  
	//比对长度，然后根据长度确定比较范围，最后确定大小
	while (iterBegin != iterEnd)
	{
		hasDotRight = hasPositiveRight = hasDecimalBeginRight = hasDecimalEndRight = false;

		if ((numberRightBegin = find_if(iterBegin, iterEnd, bind(logical_and<>(), bind(greater<>(), _1, '/'), bind(less<>(), _1, ':')))) == iterEnd)
			break;

		if (!(*(numberRightBegin - 1) ^ '-'))
			//if (*(numberRightBegin - 1) == '-')
			isPositiveRight = false;
		else
			isPositiveRight = true;

		numberRightEnd = find_if_not(numberRightBegin + 1, iterEnd, bind(logical_and<>(), bind(greater<>(), _1, '/'), bind(less<>(), _1, ':')));

		if (distance(numberRightEnd, iterEnd) > 2 && *numberRightEnd == '.' && (!((*(numberLeftEnd + 1) - '/') >> 31) && ((*(numberRightEnd + 1) - ':') >> 31)))
			//if (distance(numberRightEnd, iterEnd) > 2 && *numberRightEnd == '.' && (*(numberRightEnd + 1) >= '0' && *(numberRightEnd + 1) <= '9'))
		{
			hasDotRight = true;
			numberRightDot = numberRightEnd;
			numberRightEnd = find_if_not(numberRightDot + 1, iterEnd, bind(logical_and<>(), bind(greater<>(), _1, '/'), bind(less<>(), _1, ':')));
		}



		//  首先比较符号  +  -
		if ((isPositiveLeft ^ isPositiveRight))
			//if (isPositiveLeft != isPositiveRight)
		{
			if (isPositiveLeft && !isPositiveRight)
			{
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
			if (!isPositiveLeft && isPositiveRight)
			{
				hasDotLeft = hasDotRight;
				hasPositiveLeft = hasPositiveRight;
				isPositiveLeft = isPositiveRight;
				hasDecimalBeginLeft = hasDecimalBeginRight;
				hasDecimalEndLeft = hasDecimalEndRight;
				numberLeftBegin = numberRightBegin;
				if (hasDotLeft)
					numberLeftDot = numberRightDot;
				numberLeftEnd = numberRightEnd;
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
		}

		//判断整数长度
		if (!hasPositiveLeft)
		{
			PositiveBeginLeft = find_if(numberLeftBegin, hasDotLeft ? numberLeftDot : numberLeftEnd, bind(not_equal_to<>(), _1, '0'));
			PositiveLenLeft = distance(PositiveBeginLeft, hasDotLeft ? numberLeftDot : numberLeftEnd);
			hasPositiveLeft = true;
		}


		PositiveBeginRight = find_if(numberRightBegin, hasDotRight ? numberRightDot : numberRightEnd, bind(not_equal_to<>(), _1, '0'));
		PositiveLenRight = distance(PositiveBeginRight, hasDotRight ? numberRightDot : numberRightEnd);
		hasPositiveRight = true;


		//比对整数部分长度
		if ((PositiveLenLeft ^ PositiveLenRight))
			//if (PositiveLenLeft != PositiveLenRight)
		{

			if (((PositiveLenLeft - PositiveLenRight) >> 31))
				//if (PositiveLenLeft < PositiveLenRight)   //  <
			{
				if (isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//if (hasPositiveLeft)
					//{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
			else          //  PositiveLenLeft > PositiveLenRight    >
			{
				if (!isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
		}

		//长度一致比对大小,有整数，无整数
		if (!(PositiveLenLeft ^ PositiveLenRight))
			//if (PositiveLenLeft == PositiveLenRight)
		{
			if (PositiveLenLeft)        // 有整数情况  无整数情况意味大家一样大
			{
				matchPair = mismatch(PositiveBeginLeft, hasDotLeft ? numberLeftDot : numberLeftEnd, PositiveBeginRight, hasDotRight ? numberRightDot : numberRightEnd);
				if (matchPair.first != (hasDotLeft ? numberLeftDot : numberLeftEnd))        // 相等时整数大小一致，只考虑不想等情况
				{
					if (!((*matchPair.first - *matchPair.second) >> 31))
						//if (*matchPair.first > *matchPair.second)
					{
						if (!isPositiveRight)
						{
							hasDotLeft = hasDotRight;
							hasPositiveLeft = hasPositiveRight;
							isPositiveLeft = isPositiveRight;
							hasDecimalBeginLeft = hasDecimalBeginRight;
							hasDecimalEndLeft = hasDecimalEndRight;
							numberLeftBegin = numberRightBegin;
							if (hasDotLeft)
								numberLeftDot = numberRightDot;
							//			if (hasPositiveLeft)
							//			{
							PositiveBeginLeft = PositiveBeginRight;
							PositiveLenLeft = PositiveLenRight;
							//			}
							numberLeftEnd = numberRightEnd;
						}
						iterBegin = numberRightEnd;
						if (iterBegin != iterEnd)
							++iterBegin;
						continue;
					}
					else                      //        *matchPair.first < *matchPair.second
					{
						if (isPositiveRight)
						{
							hasDotLeft = hasDotRight;
							hasPositiveLeft = hasPositiveRight;
							isPositiveLeft = isPositiveRight;
							hasDecimalBeginLeft = hasDecimalBeginRight;
							hasDecimalEndLeft = hasDecimalEndRight;
							numberLeftBegin = numberRightBegin;
							if (hasDotLeft)
								numberLeftDot = numberRightDot;
							//			if (hasPositiveLeft)
							//			{
							PositiveBeginLeft = PositiveBeginRight;
							PositiveLenLeft = PositiveLenRight;
							//			}
							numberLeftEnd = numberRightEnd;
						}
						iterBegin = numberRightEnd;
						if (iterBegin != iterEnd)
							++iterBegin;
						continue;
					}
				}
			}
		}




		//   比对小数情况
		//   首先比对首个不为0的位置长度情况   
		//   首先考虑有没有小数
		if (!hasDotLeft && !hasDotRight)  //都没有小数
		{
			iterBegin = numberRightEnd;
			if (iterBegin != iterEnd)
				++iterBegin;
			continue;
		}


		// 假设有小数，那么需要考虑若干种情况：
		// 1 一个有小数，另一个无  有小数者 可能是全0  不全为0

		if (!hasDecimalBeginLeft)
		{
			if (hasDotLeft)
			{
				DecimalBeginLeft = find_if(numberLeftDot + 1, numberLeftEnd, bind(not_equal_to<>(), _1, '0'));
			}
			DecimalBeginLenLeft = hasDotLeft ? (DecimalBeginLeft != numberLeftEnd ? distance(numberLeftDot, DecimalBeginLeft) : 0) : 0;
			hasDecimalBeginLeft = true;
		}

		if (hasDotRight)
		{
			DecimalBeginRight = find_if(numberRightDot + 1, numberRightEnd, bind(not_equal_to<>(), _1, '0'));
		}
		DecimalBeginLenRight = hasDotRight ? (DecimalBeginRight != numberRightEnd ? distance(numberRightDot, DecimalBeginRight) : 0) : 0;
		hasDecimalBeginRight = true;


		//如果都为0，也就是说无小数部分
		if (!DecimalBeginLenLeft && !hasDecimalBeginRight)
		{
			iterBegin = numberRightEnd;
			if (iterBegin != iterEnd)
				++iterBegin;
			continue;
		}




		//   首先考虑长度不一致的情况，距离越小者越大
		if ((DecimalBeginLenLeft ^ DecimalBeginLenRight))
			//if (DecimalBeginLenLeft != DecimalBeginLenRight)
		{
			if (!DecimalBeginLenLeft && hasDecimalBeginRight)   //  <
			{
				if (isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
			if (DecimalBeginLenLeft && !hasDecimalBeginRight)   //   >
			{
				if (!isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
			if (((DecimalBeginLenLeft - DecimalBeginLenRight) >> 31))
				//if (DecimalBeginLenLeft < DecimalBeginLenRight)
			{
				if (isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
			else     //   DecimalBeginLenLeft > DecimalBeginLenRight
			{
				if (!isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
		}


		//   DecimalEndLeft   DecimalEndRight   DecimalEndLenLeft   DecimalEndLenRight 
		//  小数区间比较，经过上面的比对筛选，剩下都是有小数的
		//  只考虑三种情况  大  小  等于      
		if (!DecimalEndLenLeft)
		{
			DecimalEndLeft = find_if(make_reverse_iterator(numberLeftEnd), make_reverse_iterator(DecimalBeginLeft), bind(not_equal_to<>(), _1, '0')).base() + 1;
			DecimalEndLenLeft = distance(DecimalBeginLeft, DecimalEndLeft);
			DecimalEndLenLeft = true;
		}


		DecimalEndRight = find_if(make_reverse_iterator(numberRightEnd), make_reverse_iterator(DecimalBeginRight), bind(not_equal_to<>(), _1, '0')).base() + 1;
		DecimalEndLenRight = distance(DecimalBeginRight, DecimalEndRight);
		DecimalEndLenRight = true;


		if (!(DecimalEndLenLeft ^ DecimalEndLenRight))
			//if (DecimalEndLenLeft == DecimalEndLenRight)
		{
			matchPair = mismatch(DecimalBeginLeft, DecimalEndLeft, DecimalBeginRight, DecimalEndRight);
			if (matchPair.first == DecimalEndLeft)
			{
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
			if (!((*matchPair.first - *matchPair.second) >> 31))
				//if (*matchPair.first > *matchPair.second)     //    
			{
				if (!isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					//		if (hasDecimalEndLeft)
					//		{
					DecimalEndLeft = DecimalEndRight;
					DecimalEndLenLeft = DecimalEndLenRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
			else    //   *matchPair.first < *matchPair.second     //
			{
				if (!isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					//		if (hasDecimalEndLeft)
					//		{
					DecimalEndLeft = DecimalEndRight;
					DecimalEndLenLeft = DecimalEndLenRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
		}
		if (((DecimalEndLenLeft - DecimalEndLenRight) >> 31))
			//if (DecimalEndLenLeft < DecimalEndLenRight)
		{
			matchPair = mismatch(DecimalBeginLeft, DecimalEndLeft, DecimalBeginRight, DecimalBeginRight + DecimalEndLenLeft);
			if (matchPair.first == DecimalEndLeft)   //   <
			{
				if (!isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					//		if (hasDecimalEndLeft)
					//		{
					DecimalEndLeft = DecimalEndRight;
					DecimalEndLenLeft = DecimalEndLenRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
			if (!((*matchPair.first - *matchPair.second) >> 31))
				//if (*matchPair.first > *matchPair.second)     //    
			{
				if (!isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					//		if (hasDecimalEndLeft)
					//		{
					DecimalEndLeft = DecimalEndRight;
					DecimalEndLenLeft = DecimalEndLenRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
			else    //   *matchPair.first < *matchPair.second     //
			{
				if (!isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					//		if (hasDecimalEndLeft)
					//		{
					DecimalEndLeft = DecimalEndRight;
					DecimalEndLenLeft = DecimalEndLenRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
		}
		if (!((DecimalEndLenLeft - DecimalEndLenRight) >> 31))
			//if (DecimalEndLenLeft > DecimalEndLenRight)
				//else    //   DecimalEndLenLeft > DecimalEndLenRight
		{
			matchPair = mismatch(DecimalBeginLeft, DecimalBeginLeft + DecimalEndLenRight, DecimalBeginRight, DecimalEndRight);
			if (matchPair.first == DecimalEndLeft)   //   >
			{
				if (!isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					//		if (hasDecimalEndLeft)
					//		{
					DecimalEndLeft = DecimalEndRight;
					DecimalEndLenLeft = DecimalEndLenRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
			if (!((*matchPair.first - *matchPair.second) >> 31))
				//if (*matchPair.first > *matchPair.second)     //    
			{
				if (!isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					//		if (hasDecimalEndLeft)
					//		{
					DecimalEndLeft = DecimalEndRight;
					DecimalEndLenLeft = DecimalEndLenRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
			else    //   *matchPair.first < *matchPair.second     //
			{
				if (!isPositiveRight)
				{
					hasDotLeft = hasDotRight;
					hasPositiveLeft = hasPositiveRight;
					isPositiveLeft = isPositiveRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					hasDecimalEndLeft = hasDecimalEndRight;
					numberLeftBegin = numberRightBegin;
					if (hasDotLeft)
						numberLeftDot = numberRightDot;
					//		if (hasPositiveLeft)
					//		{
					PositiveBeginLeft = PositiveBeginRight;
					PositiveLenLeft = PositiveLenRight;
					//		}
					//		if (hasDecimalBeginLeft)
					//		{
					DecimalBeginLeft = DecimalBeginRight;
					hasDecimalBeginLeft = hasDecimalBeginRight;
					//		}
					//		if (hasDecimalEndLeft)
					//		{
					DecimalEndLeft = DecimalEndRight;
					DecimalEndLenLeft = DecimalEndLenRight;
					//		}
					numberLeftEnd = numberRightEnd;
				}
				iterBegin = numberRightEnd;
				if (iterBegin != iterEnd)
					++iterBegin;
				continue;
			}
		}
	}


	if (hasDotLeft)
	{
		accumulate(make_reverse_iterator(numberLeftDot), make_reverse_iterator(numberLeftBegin), &result, [&index, &num](auto *sum, auto const ch)
		{
			if (++index)num *= 10.0f;
			*sum += (ch - '0')*num;
			return sum;
		});
		num = 0.1f;
		index = -1;
		accumulate(numberLeftDot + 1, numberLeftEnd, &result, [&index, &num](auto *sum, auto const ch)
		{
			if (++index)num *= 0.1f;
			*sum += (ch - '0')*num;
			return sum;
		});
		if (distance(source.cbegin(), numberLeftBegin) && *(numberLeftBegin - 1) == '-')
			result *= -1.0f;
		return;
	}
	accumulate(make_reverse_iterator(numberLeftEnd), make_reverse_iterator(numberLeftBegin), &result, [&index, &num](auto *sum, auto const ch)
	{
		if (++index)num *= 10.0f;
		*sum += (ch - '0')*num;
		return sum;
	});
	if (distance(source.cbegin(), numberLeftBegin) && *(numberLeftBegin - 1) == '-')
		result *= -1.0f;
	return;
}