#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
#include<iterator>
#include<functional>
#include<map>
#include<memory>
#include<any>
#include<sstream>
#include<list>

using std::list;
using std::logical_or;
using std::logical_and;
using std::shared_ptr;
using std::any;
using std::bind;
using std::not_equal_to;
using std::equal_to;
using std::placeholders::_1;
using std::string;
using std::find;
using std::search;
using std::find_if;
using std::copy;
using std::ostream_iterator;
using std::cout;
using std::adjacent_find;
using std::vector;
using std::pair;
using std::make_pair;
using std::unique_ptr;
using std::any_cast;
using std::stringstream;
using std::all_of;


static stringstream sstr;



struct Node
{
	void insertNode(const shared_ptr<Node> &svgNode, list<string> &topName, string &top, vector<pair<string, any>> &valueVec)
	{
		if (svgNode)
		{
			if (!topName.empty())
			{
				if (svgNode->top == topName.front())
				{
					topName.pop_front();
					if (!topName.empty())
					{
						if (!svgNode->child.empty())
						{
							if (topName.size()>1 || (topName.size()==1 && topName.back()!= top))
							{
								auto iter{ find_if(svgNode->child.rbegin(), svgNode->child.rend(), [&topName](auto const &everynode)
								{
									return everynode->top == topName.front();
								})};
								if (iter != svgNode->child.rend())
								{
									insertNode(*iter, topName, top, valueVec);
								}
								else
								{
									shared_ptr<Node>ptr{ new Node() };
									ptr->top.swap(top);
									ptr->value.swap(valueVec);
									svgNode->child.emplace_back(ptr);
								}
							}
							else
							{
								shared_ptr<Node>ptr{ new Node() };
								ptr->top.swap(top);
								ptr->value.swap(valueVec);
								svgNode->child.emplace_back(ptr);
							}
						}
						else
						{
							shared_ptr<Node>ptr{ new Node() };
							ptr->top.swap(top);
							ptr->value.swap(valueVec);
							svgNode->child.emplace_back(ptr);
						}
					}
					else
					{
						shared_ptr<Node>ptr{ new Node() };
						ptr->top.swap(top);
						ptr->value.swap(valueVec);
						svgNode->child.emplace_back(ptr);
					}
				}
			}
		}
	}

	string top;                               //每层开始的层名
	vector<pair<string, any>>value;           //该层数值
	vector<shared_ptr<Node>>child;            //子节点
};


void split(const string & source, vector<string> &vec, const string &word)
{
	if (!source.empty() && !word.empty())
	{
		vec.clear();
		string::const_iterator iterTempBegin{ source.begin() }, iterTempEnd{ source.begin() }, iterEnd{ source.end() };
		string temp;
		while (iterTempBegin != iterEnd)
		{
			iterTempEnd = search(iterTempEnd, iterEnd, word.begin(), word.end());

			if (distance(iterTempBegin, iterTempEnd))
			{
				temp.assign(iterTempBegin, iterTempEnd);
				vec.emplace_back(temp);
			}

			if (iterTempEnd == iterEnd)
				break;

			iterTempBegin = (iterTempEnd += word.size());
		}
	}
}


void travelSVG(const shared_ptr<Node> &svg)
{
	if (svg)
	{
		if (!svg->top.empty())
			sstr << svg->top << ' ';
		if (!svg->value.empty())
		{
			for (auto const &myPair : svg->value)
			{
				sstr << myPair.first << " = ";
				if (myPair.second.type() == typeid(string))
				{
					sstr << any_cast<string>(myPair.second) << "   ";
				}
				else if (myPair.second.type() == typeid(vector<pair<string, string>>))
				{
					for (auto const &innerPair : any_cast<vector<pair<string, string>>>(myPair.second))
					{
						sstr << innerPair.first << ":" << innerPair.second << "  ";
					}
					sstr << "   ";
				}
			}
			sstr << '\n';
		}
		else
			sstr << '\n';
		if (!svg->child.empty())
		{
			if (svg->top != "style")
				sstr << "father is " << svg->top << " ,  child is\n";
			for (auto const &child : svg->child)
			{
				travelSVG(child);
			}
			if (svg->top != "style")
				sstr << "father  " << svg->top << "  end\n";
		}
	}
}





void preaseSVG(const string &source)
{
	if (!source.empty())
	{
		string top, word, type, name, findStr0;
		string findStr1{ "<svg" }, findStr2{ "</svg>" };
		string::const_iterator iterBegin{ source.begin() }, iterEnd{ source.end() }, svgEnd;
		string::const_iterator iterSvgBegin{ iterBegin }, iterSvgEnd;
		string::const_iterator iterTopBegin, iterTopEnd;
		string::const_iterator NameBegin, NameEnd;
		string::const_iterator iterTempBegin, iterTempEnd;
		
		vector<pair<string, vector<pair<string, string>>>>result;
		vector<pair<string, any>>valueVec;
		vector<pair<string, string>>innerValue;
		vector<string>vec; ;
		string::const_iterator iter;
		list<string>topName, vecTemp;       //记录层级
		shared_ptr<Node>svgNode{};

		while (iterSvgBegin != iterEnd)   //处理整段字符串
		{
			if ((iterSvgBegin = search(iterSvgBegin, iterEnd, findStr1.begin(), findStr1.end())) == iterEnd)
				break;

			if ((iterSvgEnd = search(iterSvgBegin + findStr1.size(), iterEnd, findStr2.begin(), findStr2.end())) == iterEnd)
				break;

			iterSvgEnd += findStr2.size();

			//copy(iterSvgBegin, iterSvgEnd, ostream_iterator<char>(sstr));
			//sstr << '\n';

			while (iterSvgBegin != iterSvgEnd)         //处理整段svg字符串
			{
				if ((iterTopBegin = find(iterSvgBegin, iterSvgEnd, '<')) == iterSvgEnd)
					break;

				if ((iterTopEnd = find(iterTopBegin + 1, iterSvgEnd, '>')) == iterSvgEnd)
					break;

				//copy(iterTopBegin, iterTopEnd+1, ostream_iterator<char>(sstr));
				//sstr << '\n';

				while (iterTopBegin != iterTopEnd)      //处理整段top
				{
					NameBegin = iterTopBegin;
					if (NameBegin + 1 == iterTopEnd)
						break;

					++NameBegin;
					if ((NameBegin = find_if(NameBegin, iterTopEnd, bind(not_equal_to<>(), _1, ' '))) == iterTopEnd)
						break;

					//判断topName
					if ((NameEnd = find(NameBegin, iterTopEnd, ' ')) == iterTopEnd)
					{
						if (distance(NameBegin, NameEnd))
						{
							//copy(NameBegin, NameEnd , ostream_iterator<char>(sstr));
							//sstr << '\n';

							if (*NameBegin == '/' && !topName.empty())
								topName.pop_back();
							else
							{
								top.assign(NameBegin, NameEnd);
								findStr0.assign("</" + top + ">");
								if (search(NameEnd, iterSvgEnd, findStr0.begin(), findStr0.end()) != iterSvgEnd)
									topName.emplace_back(top);
								if (!topName.empty())
								{
									vecTemp.assign(topName.begin(), topName.end());
									svgNode->insertNode(svgNode, vecTemp, top, valueVec);
								}
							}
						}
						break;
					}

					//copy(NameBegin, NameEnd + 1, ostream_iterator<char>(sstr));
					//sstr << '\n';

					if (*NameBegin == '/' && !topName.empty())
					{
						topName.pop_back();
					}
					else if (*NameBegin == '!')
					{
						iterTopBegin = iterTopEnd;
						continue;
					}
					else
					{
						top.assign(NameBegin, NameEnd);
						findStr0.assign("</" + top + ">");
						if (search(NameEnd, iterSvgEnd, findStr0.begin(), findStr0.end()) != iterSvgEnd)
							topName.emplace_back(top);
					}

					while (NameBegin != iterTopEnd)
					{
						if ((NameBegin = find_if(NameEnd, iterTopEnd, bind(logical_and<>(),bind(not_equal_to<>(), _1, ' '),bind(not_equal_to<>(),_1,'"')))) == iterTopEnd)
							break;

						if ((NameEnd = find(NameBegin, iterTopEnd, '=')) == iterTopEnd)
							break;

						name.assign(NameBegin, NameEnd);
						//sstr << name<< '\n';

						if ((NameBegin = find(NameEnd, iterTopEnd, '"')) == iterTopEnd)
							break;

						if ((NameEnd = find(NameBegin + 1, iterTopEnd, '"')) == iterTopEnd)
							break;

						word.assign(NameBegin + 1, NameEnd);
						if (name != "style")
						{
							valueVec.emplace_back(make_pair(name, word));
						}
						else
						{
							split(word, vec, ";");
							if (!vec.empty())
							{
								for (auto const &str : vec)
								{
									if ((iter = find(str.begin(), str.end(), ':')) == str.end())
										continue;
									innerValue.emplace_back(make_pair(string(str.begin(), iter), string(iter + 1, str.end())));
								}
								valueVec.emplace_back(make_pair(name, innerValue));
								innerValue.clear();
							}
						}

						++NameEnd;
						NameBegin = find_if(NameEnd, iterTopEnd, bind(logical_and<>(),bind(not_equal_to<>(),_1, ' '),bind(not_equal_to<>(),_1,'"')));
					}
					if ((iterTempBegin = find(NameBegin, iterSvgEnd, '>')) != iterSvgEnd)
					{
						if ((iterTempEnd = find(iterTempBegin, iterSvgEnd, '<')) != iterSvgEnd)
						{
							if (distance(iterTempBegin, iterTempEnd) > 1 && !all_of(iterTempBegin + 1, iterTempEnd, bind(logical_or<>(), bind(equal_to<>(), _1, '\t'),bind(equal_to<>(),_1,' '))))
							{
								word.assign(iterTempBegin + 1, iterTempEnd);
								valueVec.emplace_back(make_pair(top, word));
							}
						}
					}
					if (top == "svg")
					{
						svgNode.reset(new Node());
						svgNode->top.swap(top);
						svgNode->value.swap(valueVec);
					}
					else
					{
						if (!topName.empty())
						{
							vecTemp.assign(topName.begin(), topName.end());
							svgNode->insertNode(svgNode, vecTemp, top, valueVec);
						}
					}
					iterTopBegin = find(NameEnd, iterTopEnd, '<');
				}

				iterSvgBegin = iterTopEnd;
			}
			iterSvgBegin = iterSvgEnd;
		}
		sstr << "\n\nsvg begin\n";
		travelSVG(svgNode);
		sstr << "svg end\n\n";
		cout << sstr.str();
		sstr.str("");
		sstr.clear();
	}
}










int main()
{
	vector<string>vec{// "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\"><circle cx=\"100\" cy=\"50\" r=\"40\" stroke=\"black\" stroke-width=\"2\" fill=\"red\" /></svg>" ,
//	 "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\"><rect width=\"300\" height=\"100\" style=\"fill:rgb(0,0,255);stroke-width:1;stroke:rgb(0,0,0)\"/></svg>",
//		"<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\"><ellipse cx=\"240\" cy=\"100\" rx=\"220\" ry=\"30\" style=\"fill:purpl\"/><ellipse cx=\"220\" cy=\"70\" rx=\"190\" ry=\"20\" style=\"fill:lime\"/><ellipse cx=\"210\" cy=\"45\" rx=\"170\" ry=\"15\" style=\"fill:yellow\"/></svg>",
//	"<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\"><line x1=\"0\" y1=\"0\" x2=\"200\" y2=\"200\" style=\"stroke:rgb(255,0,0);stroke-width:2\"/></svg>",
//		"<svg height=\"210\" width=\"500\"><polygon points=\"200,10 250,190 160,210\" style=\"fill:lime;stroke:purple;stroke-width:1\"/></svg>",
//		"<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\"><polyline points=\"20,20 40,25 60,40 80,120 120,140 200,180\" style=\"fill:none;stroke:black;stroke-width:3\"/></svg>",
//		"<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\"><path d=\"M150 0 L75 200 L225 200 Z\" /></svg>",
//		"<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\">\
//  <g fill=\"none\">\
//	<path stroke=\"red\" d=\"M5 20 l215 0\" />\
//	<path stroke=\"blue\" d=\"M5 40 l215 0\" />\
//	<path stroke=\"black\" d=\"M5 60 l215 0\" />\
//  </g>\
//</svg>",
//		"<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\">\
//  <text x=\"0\" y=\"15\" fill=\"red\" transform=\"rotate(30 20,40)\">I love SVG</text>\
//</svg>",
//		"<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\">\
//  <text x=\"10\" y=\"20\" style=\"fill:red;\">Several lines:\
//	<tspan x=\"10\" y=\"45\">First line</tspan>\
//	<tspan x=\"10\" y=\"70\">Second line</tspan>\
//  </text>\
//</svg>",
//		"<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" \
//xmlns:xlink=\"http://www.w3.org/1999/xlink\">\
//  <a xlink:href=\"http://www.w3schools.com/svg/\" target=\"_blank\">\
//	<text x=\"0\" y=\"15\" fill=\"red\">I love SVG</text>\
//  </a>\
//</svg>",


//		"<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\"\
//xmlns:xlink=\"http://www.w3.org/1999/xlink\">\
//   <defs>\
//	<path id=\"path1\" d=\"M75,20 a1,1 0 0,0 100,0\" />\
//  </defs>\
//  <text x=\"10\" y=\"100\" style=\"fill:red;\">\
//	<textPath xlink:href=\"#path1\">I love SVG I love SVG</textPath>\
//  </text>\
//</svg>",


/*		"<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\">\
  <path id=\"lineAB\" d=\"M 100 350 l 150 -300\" stroke=\"red\"\
  stroke-width=\"3\" fill=\"none\"/>\
  <path id=\"lineBC\" d=\"M 250 50 l 150 300\" stroke=\"red\"\
  stroke-width=\"3\" fill=\"none\" />\
  <path d=\"M 175 200 l 150 0\" stroke=\"green\" stroke-width=\"3\"\
  fill=\"none\" />\
  <path d=\"M 100 350 q 150 -300 300 0\" stroke=\"blue\"\
  stroke-width=\"5\" fill=\"none\" />\
  <!--Mark relevant points-->\
  <g stroke=\"black\" stroke-width=\"3\" fill=\"black\">\
	<circle id=\"pointA\" cx=\"100\" cy=\"350\" r=\"3\" />\
	<circle id=\"pointB\" cx=\"250\" cy=\"50\" r=\"3\" />\
	<circle id=\"pointC\" cx=\"400\" cy=\"350\" r=\"3\" />\
  </g>\
  <!--Label the points-->\
  <g font-size=\"30\" font=\"sans-serif\" fill=\"black\" stroke=\"none\"\
  text-anchor=\"middle\">\
	<text x=\"100\" y=\"350\" dx=\"-30\">A</text>\
	<text x=\"250\" y=\"50\" dy=\"-10\">B</text>\
	<text x=\"400\" y=\"350\" dx=\"30\">C</text>\
  </g>\
</svg>", */

//		"<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\">\
//  <defs>\
//	<filter id=\"f1\" x=\"0\" y=\"0\">\
//	  <feGaussianBlur in=\"SourceGraphic\" stdDeviation=\"15\" />\
//	</filter>\
//  </defs>\
//  <rect width=\"90\" height=\"90\" stroke=\"green\" stroke-width=\"3\"\
//  fill=\"yellow\" filter=\"url(#f1)\" />\
//</svg>",

//		"<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\">\
//  <defs>\
//	<filter id=\"f1\" x=\"0\" y=\"0\" width=\"200%\" height=\"200%\">\
//	  <feOffset result=\"offOut\" in=\"SourceGraphic\" dx=\"20\" dy=\"20\" />\
//	  <feBlend in=\"SourceGraphic\" in2=\"offOut\" mode=\"normal\" />\
//	</filter>\
//  </defs>\
//  <rect width=\"90\" height=\"90\" stroke=\"green\" stroke-width=\"3\"\
//  fill=\"yellow\" filter=\"url(#f1)\" />\
//</svg>",

/*		"<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\">\
  <defs>\
	<filter id=\"f1\" x=\"0\" y=\"0\" width=\"200%\" height=\"200%\">\
	  <feOffset result=\"offOut\" in=\"SourceAlpha\" dx=\"20\" dy=\"20\" />\
	  <feGaussianBlur result=\"blurOut\" in=\"offOut\" stdDeviation=\"10\" />\
	  <feBlend in=\"SourceGraphic\" in2=\"blurOut\" mode=\"normal\" />\
	</filter>\
  </defs>\
  <rect width=\"90\" height=\"90\" stroke=\"green\" stroke-width=\"3\"\
  fill=\"yellow\" filter=\"url(#f1)\" />\
</svg>",   */

/*		"<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\">\
  <defs>\
	<filter id=\"f1\" x=\"0\" y=\"0\" width=\"200%\" height=\"200%\">\
	  <feOffset result=\"offOut\" in=\"SourceGraphic\" dx=\"20\" dy=\"20\" />\
	  <feColorMatrix result =\"matrixOut\" in=\"offOut\" type=\"matrix\"\
	  values=\"0.2 0 0 0 0 0 0.2 0 0 0 0 0 0.2 0 0 0 0 0 1 0\" />\
	  <feGaussianBlur result=\"blurOut\" in=\"matrixOut\" stdDeviation=\"10\" />\
	  <feBlend in=\"SourceGraphic\" in2=\"blurOut\" mode=\"normal\" />\
	</filter>\
  </defs>\
  <rect width=\"90\" height=\"90\" stroke=\"green\" stroke-width=\"3\"\
  fill=\"yellow\" filter=\"url(#f1)\" />\
</svg>" */
 
         "<!DOCTYPE html>\
			 <html>\
			 <body>\
\
			 <p><b>Note:</b> This example only works in Firefox and Google Chrome.</p>\
\
			 <svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\">\
			 <rect x=\"20\" y=\"20\" width\=\"250\" height=\"250\" style=\"fill:blue\">\
			 <animate attributeType=\"CSS\" attributeName=\"opacity\" from=\"1\" to=\"0\" dur=\"5s\" repeatCount=\"indefinite\" />\
			 </rect>\
			 </svg>\
\
			 </body>\
			 </html>",       


	  ""
	};


	//string test{ "fill:rgb(0,0,255);stroke-width:1;stroke:rgb(0,0,0)" };
	//vector<string>vec;
	//string word{ ";" };
	//split(test, vec, word);
	//for (auto const &i : vec)
	//{
	//	sstr << i << '\n';
	//}

	for (auto const &i : vec)
	{
		preaseSVG(i);
	}


	return 0;
}

